package se.experis.SpringWebThymeleaf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import se.experis.SpringWebThymeleaf.database.Query;
import se.experis.SpringWebThymeleaf.models.Contact;

import java.util.ArrayList;

@Controller
public class ContactApplicationController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting.html";
    }

    @GetMapping("/listcontacts")
    public String listContacts(Model model) {
        Query test = new Query();
        ArrayList<Contact> allContacts = test.getAllContactsFromDB();

        model.addAttribute("allContacts", allContacts);
        model.addAttribute("contact", new Contact());

        return "listcontacts.html";
    }

    @GetMapping("/edit")
    public String editContact(@ModelAttribute("contact") Contact contact,Model model) {
        System.out.println(contact.toString());
        Query query = new Query();
        query.editFromDB(contact.getContactId(), contact.getName(), contact.getEmail(), contact.getPhone());
        ArrayList<Contact> allContacts = query.getAllContactsFromDB();

        model.addAttribute("allContacts", allContacts);
        model.addAttribute("contact", new Contact());
        return "listcontacts.html";
    }

    @GetMapping("/remove")
    public String removeContact(@ModelAttribute("contact") Contact contact,Model model) {
        System.out.println(contact.toString());
        Query query = new Query();
        query.deleteFromDB(contact.getContactId());
        ArrayList<Contact> allContacts = query.getAllContactsFromDB();

        model.addAttribute("allContacts", allContacts);
        model.addAttribute("contact", new Contact());
        return "listcontacts.html";
    }

    @GetMapping("/searchcontact")
    public String searchContacts() {
        return "searchcontact.html";
    }

    @PostMapping("/searchcontact")
    public String update(@RequestParam(value="input") String input, Model model) {
        Query query = new Query();
        ArrayList<Contact> list = query.searchFullList(input);
        if(list!= null) {
            model.addAttribute("list", list);
        } else {
//            model.addAttribute("")
        }
        return "searchcontact.html";
    }


    @GetMapping("/addcontact")
    public String addContact(Model model) {

        model.addAttribute("contact", new Contact());
        return "addcontact.html";
    }

    @PostMapping("/redirect")
    public String redirect(@ModelAttribute("contact") Contact contact) {
        Query test = new Query();
        test.addToDB(contact);

        return "redirect:/addcontact?name=" + contact.getName();
    }

}
