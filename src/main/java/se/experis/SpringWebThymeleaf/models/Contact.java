package se.experis.SpringWebThymeleaf.models;

public class Contact {

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String contactId, name, email, phone;

    public Contact(String contactId, String contactName, String email, String phone) {
        this.contactId = contactId;
        this.name = contactName;
        this.email = email;
        this.phone = phone;
    }

    public Contact(String contactName, String email, String phone) {
        this.name = contactName;
        this.email = email;
        this.phone = phone;
    }

    public Contact() {

    }

    public String getContactId() {
        return contactId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "contactId='" + contactId + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
