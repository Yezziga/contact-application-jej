package se.experis.SpringWebThymeleaf.database;
import java.sql.*;

/**
 * A singleton class that opens a connection to the database.
 * @author Jessica Quach
 */
public final class JDBC {
    private static Connection conn = null;
    private static final String URL = "jdbc:sqlite:Contacts_Database.db";

    /**
     * Private constructor.
     */
    private JDBC() {
        conn = getConnection();
    }

    /**
     * Establishes a connection to the database and returns the connection
     * @return a Connection to the database
     */
    public static Connection getConnection() {
        try {
            if(conn == null || conn.isClosed()) {
                conn = DriverManager.getConnection(URL);
                System.out.println("Connection to SQLite has been established.");
            } else {
                System.out.println("Already connected");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * Creates a database with the URL
     */
    public void createNewDB() {
        try {
            conn = DriverManager.getConnection(URL);
            if(conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
