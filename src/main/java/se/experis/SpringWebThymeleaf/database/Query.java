package se.experis.SpringWebThymeleaf.database;

import se.experis.SpringWebThymeleaf.models.Contact;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Class for querying data from the database.
 * @author Jessica Quach
 */
public class Query {
    private final String TABLE_NAME = "Contact";
    private final String CONTACT_ID_COLUMN = "contact_id";
    private final String NAME_COLUMN = "full_name";
    private final String EMAIL_COLUMN = "email";
    private final String PHONE_NUMBER_COLUMN = "contact_number";
    private AtomicLong nextId = new AtomicLong();
    private Connection conn;

    /**
     * Creates the table "Contact" if it does not already exist.
     * Not used by the application, but used by devs if needed
     * JQ
     */
    public void createTable() {
        try {
            conn = JDBC.getConnection();
            PreparedStatement statement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                            CONTACT_ID_COLUMN + " TEXT PRIMARY KEY," +
                            NAME_COLUMN + " TEXT NOT NULL," +
                            EMAIL_COLUMN + " TEXT NOT NULL," +
                            PHONE_NUMBER_COLUMN + " TEXT NOT NULL)");
            statement.execute();
            System.out.println("Table " + TABLE_NAME + " created.");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Adds the given contact to the database. The contact gets a randomized id.
     * JQ
     * @param contact the contact to add to the database
     */
    public void addToDB(Contact contact) {
        try {
            conn = JDBC.getConnection();
            PreparedStatement statement = conn.prepareStatement(
                    "INSERT INTO " +  TABLE_NAME + " VALUES (" + randomizeID() + ", ?, ?, ?)");
            statement.setString(1, contact.getName());
            statement.setString(2, contact.getEmail());
            statement.setString(3, contact.getPhone());
            statement.execute();
            System.out.println("Contact added into " + TABLE_NAME);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Queries for every contact in the database. Puts them in an ArrayList
     * and returns it.
     * @return a list of contacts
     */
    public ArrayList<Contact> getAllContactsFromDB() {
        ArrayList<Contact> list = new ArrayList<>();
        try {
            conn = JDBC.getConnection();
            PreparedStatement statement = conn.prepareStatement(
                    "SELECT* FROM " +  TABLE_NAME);

            ResultSet resultSet= statement.executeQuery();

            while(resultSet.next()) {
                list.add(new Contact(
                        resultSet.getString(CONTACT_ID_COLUMN),
                        resultSet.getString(NAME_COLUMN),
                        resultSet.getString(EMAIL_COLUMN), resultSet.getString(PHONE_NUMBER_COLUMN)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    /**
     * Deletes the contact with the given ID from the database
     */
    public void deleteFromDB(String id) {

        //TODO Exception if input is not in database. Also if ignore CaSINg is important
        try {
            conn = JDBC.getConnection();

            PreparedStatement statement = conn.prepareStatement(
                    "DELETE FROM " + TABLE_NAME + " WHERE " + CONTACT_ID_COLUMN + "= ?");
            statement.setString(1, id);
            statement.execute();

            System.out.println("Deleted the contact: " + id + " in: " + TABLE_NAME);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Searches through the database and gets all the contacts with the same name
     * as the given input. Ignores case.
     * JQ
     * @param userInput name of contact to retrieve
     * @return a list of contacts with the same name as input
     */
    public ArrayList<Contact> searchFullList(String userInput) {
        ArrayList<Contact> list = getAllContactsFromDB();
        ArrayList<Contact> newList = null;

        // Scrolling through the full list and printing if there's a match, not case-sensitive!
        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).getName().equalsIgnoreCase(userInput)){
//                System.out.println(list.get(i).toString());
                if(newList==null) {
                    newList = new ArrayList<>();
                }
                newList.add(list.get(i));
            }
        }
        return newList;
    }

    /**
     * Edits the contact with the given id.
     * @param id
     * @param name
     * @param email
     * @param phone
     */
    public void editFromDB(String id, String name, String email, String phone) {

        try {
            conn = JDBC.getConnection();

            PreparedStatement statement = conn.prepareStatement(
                    "UPDATE " + TABLE_NAME + " SET " + NAME_COLUMN + "= ?, " + EMAIL_COLUMN + " =?, "
                            + PHONE_NUMBER_COLUMN + "=? WHERE " + CONTACT_ID_COLUMN  + "=?");
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, phone);
            statement.setString(4, id);

            statement.execute();

            System.out.println("Edited contact with id: " + id);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Randomizes a value. Used for setting PK id to contacts
     * in database. Does not ensure uniqueness
     * @return a randomized value between w-Integer.MAX_VALUE
     */
    public int randomizeID() {
        Random ran = new Random();
        return  ran.nextInt(Integer.MAX_VALUE);
    }

    public static void main(String[] args) {
        Query query = new Query();
      //  query.createTable();
//        ArrayList<Contact> list = query.searchFullList("kALLE");
//        if(list==null) {
//            System.out.println("empty list");
//        }
//        System.out.println("Contacts named \"Kalle\"");
//        for (Contact c : list) {
//            System.out.println( c.toString());
//        }
//        System.out.println();

    }




}
