Project made by Jessica, Erik & Jacob


------ Contributions ------
Jessica
DB: creation of & connection to db, table. Adding to db, getting a list of all contacts, removing & editing
Implemented the search, delete, and edit of contact function
 

Erik
Erik Johansson managed the Git Kraken for the project. He was also responsible for the Thymeleaf part of the project and created methods for listing members and adding new members as well as comtomizing the html files corresponding to this functionality.

Jacob comments:
Front-end. Focused on design of landing-page and others appealing features. (also small insition in the database-code, even though it would later seem closer to harmful.)
 Biggest focus was on organizing the elements through the <style> in headers. Making the columns etc easily copied to other classes.
Happy about the embedded video that starts automatically.
Biggest challenge was to create the lists so that data could be extracted easily.
 Found solution in .next() methods in js, but time was short.